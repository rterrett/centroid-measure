#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
author: rterrett
date: 2017-09-15
purpose: calculate distance between an atom and the centroid of some list of other atoms
'''

import sys
import math


def norm(v):
    '''Calculate vector norm of argument'''
    return(math.sqrt(sum([a**2 for a in v])))


def centroid(atoms):
    '''Calculate centroid of a collection of vectors'''
    return([sum([atom[i] for atom in atoms]) / len(atoms) for i in range(len(atoms[0]))])


def dist(v1, v2):
    '''Calculate difference of two vectors'''
    return([v - v2[i] for i, v in enumerate(v1)])


def main():
    # Read XYZ geometry augmented by a line which contains a list of atoms.
    # The program calculates the distance between the first listed atom and
    # the centroid of the remaining atom indices.
    # Here we cut off the first line, which simply contains the number of
    # atoms.
    lines = [line.strip().split()
             for line in open(sys.argv[-1]).readlines()[1:]]
    # Obtain list of atom indices (1-indexed).
    indices = map(int, [index for index in lines[0]])
    # Get atom coordinates if they are in list of indices of interest.
    data = [map(float, line[1:])
            for index, line in enumerate(lines[1:]) if index + 1 in indices]
    # Calculate distance to centroid.
    diff = dist(centroid(data[1:]), data[0])
    # Write to stdout
    sys.stdout.write('{0} pm\n'.format(round(norm(diff), 3) * 100))


if __name__ == '__main__':
    main()
